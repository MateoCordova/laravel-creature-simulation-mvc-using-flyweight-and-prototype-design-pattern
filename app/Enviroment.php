<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enviroment extends Model
{
    protected $fillable = ['id','width','heigth','color'];
    
    public function simulacion(){ 
        return $this->hasOne('App\Simulacion'); 
    }
}
