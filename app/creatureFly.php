<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreatureFly extends Model
{
    protected $fillable = ['id','x','y','simulacion_id'];
    
    public function simulacion(){ 
        return $this->belongs('App\Simulacion'); 
    }
}
