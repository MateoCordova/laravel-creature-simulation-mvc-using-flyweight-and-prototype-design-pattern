<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <style>
    canvas {
        border: 1px solid #d3d3d3;
        background-color: "<?php echo $simulacion->enviroment->color;?>";
    }
    </style>
    </head>
    <body onload="startGame()">
        <h2 id="repNum">Repeticion numero</h2>
        <h2 id="creNum">Creaturas vivas</h2>

        <div id="rightside" class="rightside">
            
        </div>
    </body>
<script>
$.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
     }
 }); 
var creatures = [];
var foods = [];
var canvas = document.createElement("canvas");
var creatureCommon = new creature(<?php echo $simulacion->creature->width; ?>,<?php echo $simulacion->creature->heigth ;?>, "<?php echo $simulacion->creature->color;?>", <?php echo ($simulacion->enviroment->width/2) - $simulacion->creature->width/2;?>, <?php echo $simulacion->enviroment->heigth/2;?>, <?php echo $simulacion->creature->velocity;?>) ;
canvas.id = "Ambiente";
function startGame() {
    enviroment.start();
    for (var i = 0; i <= <?php echo $simulacion->initial_mMonster ;?>; i++) {
        creatures.push(new creatureFly(i+1,creatureCommon,creatureCommon.xStart,creatureCommon.yStart));
    }
    for (var i = 0; i <= <?php echo $simulacion->initial_nFood ;?>; i++) {
        foods.push(new food(<?php echo $simulacion->food->width; ?>,
        <?php echo $simulacion->food->heigth; ?>,
        "<?php echo $simulacion->food->color; ?>")) ;
    }
}

var enviroment = {
    canvas : document.getElementById('rightside').appendChild(canvas),
    start : function() {
        this.canvas.width = 500;
        this.canvas.height = 500;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 41);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stop : function() {
        clearInterval(this.interval);
    },
    restart : function(){
        this.interval = setInterval(updateGameArea, 41);
    }
}
function creatureFly(id,creature,x,y){
    this.id = id;
    this.creature = creature;
    var temp1 = (Math.random() * this.creature.velocidad)-this.creature.velocidad/2;
    var temp2 = 0;
    if(Math.floor(Math.random() * 2)==0)
    {
        temp2 = Math.sqrt((this.creature.velocidad*this.creature.velocidad)-(temp1*temp1));
    } else {
        temp2 = -Math.sqrt((this.creature.velocidad*this.creature.velocidad)-(temp1*temp1));
    }
    this.velX = temp1;
    this.velY = temp2;
    this.x = x;
    this.y = y;
    this.eaten = false;  
    ctx = enviroment.context;
    ctx.fillStyle = this.creature.color;
    ctx.fillRect(this.x, this.y, this.creature.width, this.creature.height);
    this.update = function(){
        ctx = enviroment.context;
        ctx.fillStyle = this.creature.color;
        ctx.fillRect(this.x, this.y, this.creature.width, this.creature.height);
    }
    this.crashWith = function(otherobj) {
        var myleft = this.x;
        var myright = this.x + (this.creature.width);
        var mytop = this.y;
        var mybottom = this.y + (this.creature.height);
        var otherleft = otherobj.x;
        var otherright = otherobj.x + (otherobj.width);
        var othertop = otherobj.y;
        var otherbottom = otherobj.y + (otherobj.height);
        var crash = true;
        if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
            crash = false;
        }
        return crash;
    }
}
function creature(width, height, color, xStart, yStart, velocidad) {
    this.width = width;
    this.height = height;
    this.color = color;
    this.velocidad = velocidad;
    this.xStart = xStart;
    this.yStart = yStart;
}
function food(width, height, color) {
    this.width = width;
    this.height = height;
    this.x = Math.random() *enviroment.canvas.width;
    this.y =  Math.random() *enviroment.canvas.width;    
    ctx = enviroment.context;
    ctx.fillStyle = color;
    ctx.fillRect(this.x, this.y, this.width, this.height);
    this.update = function(){
        ctx = enviroment.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}
var cycle= 0;
var rep=0;
var rep2=0;
var dataJ="";
var temp=[];
function updateGameArea() {
    enviroment.clear();
    creatures.forEach(creatur => {
        if(rep>200){
            var velXaux = (Math.random() *2*creatur.creature.velocidad)-creatur.creature.velocidad;
            creatur.velX = velXaux;
            if(Math.floor(Math.random() * 2)==0)
            {
                creatur.velY = Math.sqrt((creatur.creature.velocidad*creatur.creature.velocidad)-(velXaux*velXaux));
            } else {
                creatur.velY = -Math.sqrt((creatur.creature.velocidad*creatur.creature.velocidad)-(velXaux*velXaux));
            }
            
        }
        if((creatur.x+creatur.velX)>enviroment.canvas.width ||(creatur.x+creatur.velX)<0){
            creatur.velX = creatur.velX*(-1);
        }
        if((creatur.y+creatur.velY)>enviroment.canvas.width ||(creatur.y+creatur.velY)<0){
            creatur.velY = creatur.velY*(-1);
        }
        creatur.x += creatur.velX;
        creatur.y += creatur.velY;
        if(creatur.eaten == false){
            for (var j = foods.length-1; j >= 0; j--) {
                if (creatur.crashWith(foods[j])) {
                    foods.splice(j, 1);
                    creatur.eaten = true;
                }
            }      
        }
        
        creatur.update();
    });
    if (rep > 200) {
        rep = 0;
    }
    foods.forEach(food=> {
        food.update();
    });      
    if(rep2>400){
        rep2=0;
        fillData();
        enviroment.stop();
        $.ajax({
            url:'save',
            data: dataj,
            type:'post',
            success:  function (response) {
                var k = 0;
                creatures = []
                response.forEach(element => {
                    
                    creatures.push(new creatureFly(k+1,creatureCommon,parseFloat(element.x),parseFloat(element.y)));
                    k++;
                });
                for (var i = 0; i <= <?php echo $simulacion->initial_nFood ;?>; i++) {
                    foods.push(new food(<?php echo $simulacion->food->width; ?>,
                    <?php echo $simulacion->food->heigth; ?>,
                    "<?php echo $simulacion->food->color; ?>")) ;
                }
                cycle++;

                document.getElementById('repNum').innerHTML = "Repetición Número: " + cycle;
                document.getElementById('creNum').innerHTML = "Número de Creaturas: "+  creatures.length;
                enviroment.restart();
            },
            statusCode: {
                404: function() {
                alert('web not found');
                }
            },
            error:function(x,xs,xt){
            window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
            }
        });
    }
    rep ++;
    rep2 ++;
}
function fillData(){
    dataj = {
        'N': creatures.length,
        'Xs': [],
        'Ys': [],
        'simulacion_id': 1,
        'eaten':[],
    }
    for(var j = 0; j< creatures.length; j++){
        dataj.Xs.push(creatures[j].x);
    }
    for(var j = 0; j< creatures.length; j++){
        dataj.Ys.push(creatures[j].y);
    }
    for(var j = 0; j< creatures.length; j++){
        dataj.eaten.push(creatures[j].eaten);
    }
}
</script>
<br>
</body>
</html>
