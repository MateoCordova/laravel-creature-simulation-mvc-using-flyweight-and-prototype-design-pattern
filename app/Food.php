<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = ['id','width','height','color'];
    public function simulacion(){ 
        return $this->haveMany('App\Simulacion'); 
    }
}
