<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimulacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rep');
            $table->integer('initial_mMonster');
            $table->integer('initial_nFood');
            $table->unsignedBigInteger('creature_id');
            $table->unsignedBigInteger('food_id');
            $table->unsignedBigInteger('enviroment_id');
            $table->timestamps();
            $table->foreign('creature_id')->references('id')->on('creatures');
            $table->foreign('food_id')->references('id')->on('foods');
            $table->foreign('enviroment_id')->references('id')->on('enviroments');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulacions');
    }
}
