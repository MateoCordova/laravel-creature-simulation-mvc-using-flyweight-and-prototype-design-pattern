<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Simulacion extends Model
{
    protected $fillable = ['id','rep','initial_mMonster','initial_nFood','creature_id','food_id','environment_id'];
    public function food(){ 
        return $this->belongsTo('App\Food'); 
    }
    public function creature(){ 
        return $this->belongsTo('App\Creature'); 
    }
    public function enviroment(){ 
        return $this->belongsTo('App\Enviroment'); 
    }
    public function creatureFly(){ 
        return $this->belongsTo('App\creatureFly'); 
    }
}
