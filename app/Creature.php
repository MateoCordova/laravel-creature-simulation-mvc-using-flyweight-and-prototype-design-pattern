<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creature extends Model
{
    protected $fillable = ['id','width','height','color','velocity'];
    public function simulacion(){ 
        return $this->hasOne('App\Simulacion'); 
    }
}
