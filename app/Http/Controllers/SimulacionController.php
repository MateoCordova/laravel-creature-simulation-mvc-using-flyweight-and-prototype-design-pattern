<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Creature;
use App\Environment;
use App\Food;
use App\Simulacion;
use App\CreatureFly;

class SimulacionController extends Controller
{
    public function ver(){
        $simulacion = Simulacion::find(1);
        $creatureFlies = CreatureFly::where('simulacion_id', 1)->delete();
        return view ("ver",compact("simulacion"));
    }
    public function save(Request $request){
        $list = array();
        for($i = 0; $i < $request->N; $i++)
        {
            if($request->eaten[$i]=="true"){
                $aux = new CreatureFly;
                $aux->id=$request->id[$i];
                $aux->x=$request->Xs[$i];
                $aux->y=$request->Ys[$i];
                $aux->simulacion_id=$request->simulacion_id;
                $aux->eaten=0;
                $aux2= clone $aux;
                array_push($list,$aux,$aux2);
            }
        }
        return $list;
    }
}
