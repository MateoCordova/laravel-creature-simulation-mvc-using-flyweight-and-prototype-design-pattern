<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreatureFliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creature_flies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('x');
            $table->integer('y');
            $table->unsignedBigInteger('simulacion_id');
            $table->foreign('simulacion_id')->references('id')->on('simulacions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creature_flies');
    }
}
